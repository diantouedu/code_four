# 极简版环境配置流程

说明：极简版配置以流程步骤和必要说明为主，相比本文档的完整内容，省略了讲解、部分细节、部分问题的调试方法等内容。

## windows操作系统配置

### 中文用户需改用户名为英文，英文用户名

- 左下角开始->设置->账户->家庭和其他账户->无视家庭成员、点击下方将其他人添加到这台电脑->输入英文用户名（可以不输入密码）->点击下一步直到完成创建->更改账户类型为管理员->注销或重启以登录新账户->（可选）备份并删除旧帐户。
- 方法不唯一，只要能够创建并切换到具有管理员权限的新英文名账户即可。

### 修改powershell的安全策略

- 组合键`win+r`输入`powershell`然后`ctrl`+`shift`+`Enter`以管理员权限打开，然后在powershell输入指令并同意运行：

```powershell
# 注意必须以ctrl+shift+Enter以管理员权限打开才可运行本指令
set-executionpolicy remotesigned
```

## conda 安装

### 下载

基于网络原因，国内用户推荐使用国内镜像源下载，国外用户推荐官网下载。
基于实际实用情况，学习AI的个人用户更推荐安装miniconda。

>国外推荐  
>miniconda官网下载地址：  
><https://www.anaconda.com/download/success#miniconda>  
>
>附：官网全版本下载列表：  
><https://repo.anaconda.com/miniconda/>
>
>国内推荐  
>miniconda清华镜像源下载地址：  
><https://mirror.tuna.tsinghua.edu.cn/anaconda/miniconda/?C=M&O=D>  
>注意：使用清华镜像源下载时留意conda版本，不要下载成2013年的古代版本。

### 安装

conda安装页面为英文，安装过程中会有一个单选页面和一个多选页面，具体如下：

- 单选：个人用户推荐选择“just me”安装，“all user”选项会导致后续选项不会出现add path选项。
- 多选：add path选项默认没有勾选，需要勾上，否则需要手动配置环境变量。其他选项无所谓，保持默认即可。

### 初始化

conda安装完成后先以管理员权限打开powershell

>注：打开方式与上文修改powershell安全策略的方法一致。

然后输入以下指令：

```powershell
conda init
```

关闭并重启powershell窗口，行首显示`(base)`即表示已可正常使用conda。

- 注：重启powershell窗口时若出现报错`powershell无法加载文件因为在此系统上禁止运行脚本`则说明之前没有按照之前的章节修改powershell的安全策略，修改方式参考本文开头。

### 换源

国内用户由于网络原因需要额外进行镜像源配置以更换下载源，避免出现conda下载包时龟速下载和经常性下载失败。

**注：国外网络环境的用户可以跳过本节。**

依然是win+r -> 输入powershell并打开 -> 逐行输入以下命令完成换源：

>注：  
>下方指令输入后不会有任何信息显示，是正常情况  
>下方指令输入完成后，可以输入指令`conda config --show channels`进行确认镜像源的配置是否正确  
>下方镜像源全部都要输入，而不是选择其中一个输入  
>本节使用的国内镜像源为清华大学开源软件镜像站的conda软件仓库部分，关于该镜像源的更多信息可参考下方链接获取。  
><https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/>

```powershell
# 推荐设置：下载包时显示所有镜像源
conda config --set show_channel_urls yes

# 以下为conda的清华镜像下载源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
```

## vscode 安装

### 下载与安装

在vscode官网寻找download按钮下载安装即可。  
vscode的环境变量没有存在感不需要特别注意。
>vscode官网：<https://code.visualstudio.com/>

### 安装扩展插件

- 在vscode左侧边缘点击魔方图标，或者直接使用快捷键ctrl+shift+x打开扩展页面。
- 在最上方搜索栏输入Chinese，安装带有Microsoft认证的汉化插件（第一个应该就是）。安装后需要重启vscode生效。
- 输入python，安装python插件。
- 输入jupyter，安装jupyter插件。
- 可选：安装大模型插件，安装其中一个即可：
  - Tencent Cloud AI Code Assistant腾讯云 AI 代码助手
  - Baidu Comate文心快码
  - TONGYI Lingma通义灵码
  - ChatGPT GPT-4o
  - Github Copilot
  - ······

### 推荐优化项：修改使用vscode时python的路径设置

vscode界面左下角管理(设置)->设置页的搜索栏（注意不是最顶上的搜索栏）->搜索`python.terminal.executeInFileDir`->勾上  
功能：这样可以让.py文件不在工作目录的根目录时也可以正确使用相对路径。  

### 推荐优化项：添加传参提示

搜索`python.analysis.inlayHints.callArgumentNames`->选择`all`  
功能：.py文件中的函数调用会显示函数定义时的形参，方便你检查传参有没有传歪。  
另外，搜索`inlay Hints`有更多的类似设置，你可以尝试一下。  

## 虚拟环境配置

### 创建和激活一个新的conda虚拟环境

前排提示：vscode安装完成后可以在vscode中的终端选项中直接打开powershell（powershell就是终端）。

在powershell中输入下方指令创建和激活一个conda虚拟环境：

```powershell
# 此时打开poershell后行首应显示(base)
# 如未正常显示则可能前面的步骤有疏漏
# 创建一个新环境
conda create -n test
# test为新创建的虚拟环境名，可以换成其他名称
# 注意不要多次创建同名环境以避免覆盖原环境。

# 激活创建的环境
conda activate test
# 激活后(base)变成(test)说明激活成功
```

附：常用指令

以下指令可以酌情使用

```powershell
# 列出所有的虚拟环境
conda env list

# 列出当前环境中的所有包名
conda list

# 激活与退出环境
conda activate 环境名 #激活环境
conda deactivate #退出环境

#删除环境
conda remove --name 环境名 --all
conda env remove -n 环境名 #两条指令一致
```

### 第三方库安装

- 请先确认powershell前的环境是否为test或你自定义的虚拟环境名，如果不是请先使用conda activate指令激活环境。
- 请确保powershell的行首不是`(base)`，不建议将第三方库安装到base环境中。
- 注意！如果使用`pip`安装第三方库，需要先执行`conda install python`指令。因为conda新创建的环境不包含python解释器，如果在新创建的环境使用`pip`进行安装，则第三方库会被安装到使用`conda activate`之前的环境中而不是当前激活的环境（这个是由于环境变量的比较隐性的机制导致）。

```powershell
# 请先确保powershell的行首括号**不是**(base)
conda install numpy,pandas,matplotlib,seaborn,scipy,scikit-learn,ipykernel
```

### torch安装

- torch分为GPU和CPU两个版本，二选一
  - CPU版训练速度慢、体积小，但安装简单
  - GPU版需要查询cuda版本，安装步骤可能会很麻烦，且体积更大，但训练速度更快
- 入门阶段不需要训练完整的模型，因此两种版本均可使用，请酌情选择。

#### CPU版torch安装

```powershell
# 国内已配置镜像源用户使用：
conda install pytorch torchvision torchaudio cpuonly --yes
# 国外网络环境用户使用：
conda install pytorch torchvision torchaudio cpuonly -c pytorch --yes
```

#### GPU版torch安装

- 打开powershell，输入`nvidia-smi`指令，查询自己显卡可支持的最高cuda版本（右上角）
  - 你需要选择一个低于该版本号的torch，目前pytorch安装指令中的cuda版本的三个主流选择为11.8、12.1、12.4
  - 举例：若自己在`nvidia-smi`指令中看到的的cuda版本为12.0，则只能选择cuda版本为11.8的pytorch
  - 若自己的显卡的cuda版本低于11.8，则需要去[官网相关页面](https://pytorch.org/get-started/previous-versions/)进行查阅，嫌麻烦也可改为安装CPU版torch

```powershell
# 国内已配置镜像源用户使用：
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c nvidia -c pytorch --yes
# 注意国内源极易匹配版本号失败导致安装成CPU版torch，解决方法过于复杂，遇到此问题建议暂时忽略，毕竟CPU版也能凑合

# 国外网络环境用户使用：
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c nvidia --yes
```

### 检查

pytorch安装完成后，>>>**重启powershell**<<<然后输入下方指令并稍等片刻，返回cuda支持情况。

```powershell
python -c "import torch;print(torch.cuda.is_available())"
# 安装GPU版pytorch返回应当为True  
# 安装CPU版pytorch返回应当为False  
```

```powershell
python -c "import torch;print(torch.__version__)"
# 正常情况应当返回torch版本2.x.x，过旧的cuda版本可能会是1.x.x
```

若输出符合预期则环境配置完毕。
