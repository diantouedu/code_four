# vscode的日常使用

## 指定工作文件夹

- 方法一：左上角文件->打开文件夹->选择一个文件夹作为你的工作文件夹。  
- 方法二：或点击ctrl+shift+E->点击打开文件夹（已打开一个文件夹时不会显示该按钮）。  
- 方法三：组合快捷键：先按`ctrl+k`，左下角会显示`（ctrl+k）已按下，正在等待第二个快捷键...`，此时再点击`ctrl+o`，然后选择一个文件夹。  

![方法一图片](img/open_dir.jpg)

如遇到弹窗，点击信任即可  

## 创建python或jupyter文件

快捷键ctrl+shift+E或左上角打开资源管理器，在此处点击新建文件并命名为a.py即自动被识别为python文件，jupyter文件则为.ipynb后缀。  
- 新建文件时，后缀名要手动打字输入

![方法一图片](img/new_file.jpg)

## 设置执行python文件和jupyter文件代码的python解释器（vscode内称为内核）

**注意：在终端使用conda activate 激活环境的操作是用于配置环境而不是使用环境，并不能在vscode中改变.py文件和.ipynb文件的执行环境。**

### .py文件的内核指定

- 组合键ctrl+shift+p->输入“python select interpreter”->选择conda的虚拟环境。

### .ipynb文件的内核指定

- 由于jupyter支持的是多种语言并且python内核无法直接识别.ipynb格式，所以每个jupyter文件都要独立指定python环境并安装ipykernel。
- 打开.ipynb文件->右上角选择内核->conda的虚拟环境。
- python内核在安装不能直接识别.ipynb格式文件，需要先安装对应插件。当运行代码时jupyter会提示需要安装ipykernel。
  - 国外用户点击确定即可。
  - 国内用户建议换源后手动安装：`conda install ipykernel`

## 在vscode中使用终端shell

- 中文名用户不推荐使用本操作，请使用conda自带的独立的`anaconda prompt`快捷方式（不是`anaconda powershell prompt`）
- 在vscode点击左上角`···->终端->新建终端`即可在vscode内部打开一个powershell终端，功能与使用win+r组合键单独打开的powershell页面相同，工作目录会自动修改为当前打开的工作文件夹
- 注意在终端中使用conda激活环境的操作与上述的.py与.ipynb文件的执行内核无关。
