# python环境配置基础

## 什么是python

- “python”包含python语法，python解释器两种含义。
- python解释器也叫python内核，用于执行python代码。当我们提到“下载python”时，指代的就是python解释器。
- 当我们提到“编写python代码”或“学习python”时则是指代python语法。

### python的库

- 简介  
  - 库是一种为python提供更多功能的扩展组件，参考下方举例内容。  
  - Python的库分为内置库（python自带）与第三方库（需要下载）两种。
- 常见第三方库举例：
  - torch（人工智能深度学习框架）
  - numpy（高性能科学计算）
  - pandas（数据预处理与数据清洗）
  - matplotlib（科研、数据分析时常用的绘图库）
  - seaborn（基于matplotlib的升级版数据可视化库）
  - scikit-learn（机器学习算法库）
- 使用方法：使用import语句导入库，一般都写在.py文件的开头。
- 第三方库来源：任何人都可以编写和上传到pypi以供他人使用pip下载源下载。
- 下载方法：使用pip或conda，见下文。
- 一些相关的口语表达的说明：
  - 库，包与模块的概念由于经常使用并且在不同场景、不同编程语言和框架具有不同意义，并且经常互相嵌套，因此这几个概念经常被混淆。例如“导包”、“导入库”、“导入模块”的意思常常不做区分，都理解为import操作即可。
  - 其中，python官方文档中使用的是“包（pkg）”的概念，只要一个文件夹中具有__init__.py（注意这是一个文件名而不是一行代码）文件，这个文件夹就被python认为是一个包，可以使用import语句导入。

## 背景知识：环境冲突

- 由于第三方库的编写人员团队不同（命名相互冲突）、库的版本更新、第三方库编写时可能还会依赖另一些第三方库（存在依赖关系）、BUG的常见性等各种复杂原因，在python中安装过多库易导致不同的库之间产生冲突，最终会导致代码无法正常运行，并且难以解决。这种情况被称为环境冲突。
- 越是大型、复杂、时间跨度久的项目的环境版本越是容易出现环境冲突，越是要严格按照项目说明的要求仔细配置环境，特别是版本随便换，不能图方便。

## conda虚拟环境介绍

- conda可以方便的创建、检查、隔离多个python解释器来避免环境冲突。
- 这些被conda维护的多个python解释器也叫做“虚拟环境”，在vscode中也被称为“python内核”。
- 使用conda创建“虚拟环境”的最主要便利之处有两点
  - 某个环境出问题时，不影响其他环境
  - 某个环境出问题时，方便删掉。

## conda、pip与python功能与关系介绍

- conda的功能主要有两大块，一个是虚拟环境管理，一个是包管理。
- pip的功能主要是包管理
- conda的虚拟环境管理功能可以创建、管理、删除python虚拟环境。具体点就是每个虚拟环境中都包含一个python解释器，可以独立运行python代码。conda可以创建、管理和删除这些python解释器。
- 包管理就是对每个虚拟环境中的python第三方库，进行具体配置。
- conda，pip，python的包含关系：conda的每个虚拟环境都包含一个python副本，每个python副本都包含一个pip，即：conda包含python包含pip。

参考资料：  
`https://docs.conda.org.cn/projects/conda/en/stable/commands/index.html#conda-vs-pip-vs-virtualenv-commands`

## 下载conda时的版本选择

conda分为anaconda与miniconda两种：

- anaconda内预装了许多用于科学计算和数据处理的常用库。
- miniconda仅包含conda基本功能，需要自己安装所需的库。

基于实际情况，人工智能的第三方库需求与anaconda的预装库并不契合，因此推荐使用miniconda。

## 安装conda需要下载python么

- 不建议conda和独立的python同时安装，避免conda自带的python和独立的python的环境变量出现冲突。
- 如已同时安装单独的python和conda，需要删去独立python的环境变量或将其顺序调整到底部从而避免conda的使用出现问题。
