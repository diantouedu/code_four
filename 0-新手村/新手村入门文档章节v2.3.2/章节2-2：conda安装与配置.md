# conda 安装与配置

## 下载

基于网络原因，国内用户推荐使用国内镜像源下载，国外用户推荐官网下载。

```txt
国外推荐：
miniconda官网下载地址：
https://www.anaconda.com/download/success#miniconda
附：官网全版本下载列表：
https://repo.anaconda.com/miniconda/

国内推荐：
miniconda清华镜像源下载地址：
https://mirror.tuna.tsinghua.edu.cn/anaconda/miniconda/?C=M&O=D
注意：使用清华镜像源下载时留意conda版本，不要下载成2013年的古代版本。
```

## 安装

安装时的注意事项：

- 路径可以不放在C盘，但若你打算手动配置path则要记得安装路径。
- 个人用户推荐选择“just me”安装，“all user”选项会导致后续选项不会出现add path选项。
- add PATH选项默认没有勾选，下图不推荐的原因是可能会与电脑上其他自带的python发生冲突，无视并勾选即可。  
- 其他选项随意，附选项页机翻。

![conda安装选项图](img/conda_select.png)

## 设置环境变量

首先打开高级系统设置中的环境变量

- 方法一：桌面->右键此电脑->属性->右侧高级系统设置->最下方环境变量
- 方法二：设置->左上角系统->关于->高级系统设置->环境变量
- 方法三：搜索栏搜索`path`或`编辑系统环境变量`

以上方法在各版本Windows操作系统上大致通用。  

![打开环境变量编辑](img/path_config_1.png)

然后按照下图步骤添加下方的环境变量，推荐加到最上方，特别是要在windows自带的应用商店path和其他python之前（如果有）。

```txt
你的conda安装目录
你的conda安装目录\Library\mingw-w64\bin
你的conda安装目录\Library\usr\bin
你的conda安装目录\Library\bin
你的conda安装目录\Scripts
```

![环境变量配置步骤2](img/path_config_2.png)
![环境变量配置步骤3](img/path_config_3.png)

## 初始化

初次使用conda需要执行以下步骤：

```powershell
conda init
```

关闭并重启powershell窗口，行首显示`(base)`即表示已可正常使用conda。
重启powershell窗口时若出现报错：`powershell无法加载文件因为在此系统上禁止运行脚本`说明之前没有按照之前的章节修改powershell的安全策略。
修复方法：输入指令`set-executionpolicy remotesigned`并再次重启powershell窗口。

## 换源
国内用户由于网络原因需要额外进行镜像源配置以更换下载源，避免出现conda下载包时龟速下载和经常性下载失败。

**注：国外网络环境的用户可以跳过本节。**

依然是win+r -> 输入powershell并打开 -> 逐行输入以下命令完成换源：

>注：  
>下方指令输入后不会有任何信息显示，是正常情况  
>下方指令输入完成后，可以输入指令`conda config --show channels`进行确认镜像源的配置是否正确  
>下方镜像源全部都要输入，而不是选择其中一个输入  
>本节使用的国内镜像源为清华大学开源软件镜像站的conda软件仓库部分，关于该镜像源的更多信息可参考下方链接获取。  
><https://mirrors.tuna.tsinghua.edu.cn/help/anaconda/>

```powershell
# 推荐设置：下载包时显示所有镜像源
conda config --set show_channel_urls yes

# 以下为conda的清华镜像下载源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/pytorch/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/conda-forge/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/cloud/msys2/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/r/

conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/
```

注：清华源的官方文档中对pytorch源的配置方式有误，会导致无法正常下载pytorch，这里已修正，但依然会经常出现指定cuda版本却下载成CPU版本的情况。  
相关链接：清华源官网anaconda说明页

```html
https://mirror.tuna.tsinghua.edu.cn/help/anaconda/
```

## （可选）修改虚拟环境和包缓存默认存储位置

C盘空间不足用户推荐修改虚拟环境的默认存储位置。  
本节为可选项，请根据自身电脑存储情况选择性配置。

### 修改虚拟环境保存路径

新安装conda的用户使用`conda config --add envs_dirs D:\...（由你指定）`指令即可完成修改。  

如果需要将旧环境迁移到新目录：

- 使用`conda config --show envs_dirs`指令显示所有设置的路径
- 使用`conda env list`显示所有环境和环境所在目录
- 根据上述信息，手动将旧路径的虚拟环境文件夹复制到新路径下。
- 少数情况下新建的环境依然在旧路径中，该问题的可能原因之一是conda版本低于24，先尝试更新conda为最新版本。如果更新后未能解决或无法更新conda，则只能手动迁移环境，并尝试使用`conda config --remove envs_dirs C:\...`指令删除其他路径。  

注：powershell开启时显示的路径是工作目录路径，不是虚拟环境路径，不要混淆。  
指令说明：

```powershell
conda env list # 显示已有的虚拟环境名及路径
# 老用户需要迁移已有的虚拟文件或包缓存可以先执行此指令获得虚拟环境位置，然后直接复制粘贴虚拟环境到新路径，conda会自动搜索和识别。

conda config --show envs_dirs # 显示所有默认虚拟环境保存路径
conda config --add envs_dirs D:\conda_env\envs # 增加一个虚拟环境路径
conda config --remove envs_dirs C:\... # 删除一个虚拟环境路径
conda create -n env_name --clone old_env_name #克隆环境，注意克隆指令速度极慢。本指令可作为手动复制虚拟环境的替代方案
```

### 修改包缓存路径

- 包缓存一般没有保存的必要，可以先直接使用`conda clean --all`清理缓存而不必迁移。
- 与虚拟环境路径修改方法类似，只需将上述指令中的`envs_dirs`替换为`pkgs_dirs`即可。
