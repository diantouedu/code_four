# 下载与安装

在vscode官网寻找download按钮下载安装即可。  
vscode的环境变量没有存在感不需要特别注意。

```powershell
# vscode官网：
https://code.visualstudio.com/
```

## 配置vscode

### 安装扩展插件

- 在vscode左侧边缘点击魔方图标，或者直接使用快捷键ctrl+shift+x打开扩展页面。
- 在最上方搜索栏输入Chinese，安装带有Microsoft认证的汉化插件（第一个应该就是）。安装后需要重启vscode生效。
- 输入python，安装python插件。
- 输入jupyter，安装jupyter插件。
- 可选：安装大模型插件，安装其中一个即可：
  - Tencent Cloud AI Code Assistant腾讯云 AI 代码助手
  - Baidu Comate文心快码
  - TONGYI Lingma通义灵码
  - ChatGPT GPT-4o
  - Github Copilot
  - ······

### 推荐优化项：修改使用vscode时python的路径设置

vscode界面左下角管理(设置)->设置页的搜索栏（注意不是最顶上的搜索栏）->搜索`python.terminal.executeInFileDir`->勾上  
功能：这样可以让.py文件不在工作目录的根目录时也可以正确使用相对路径。  

### 推荐优化项：添加传参提示

搜索`python.analysis.inlayHints.callArgumentNames`->选择`all`  
功能：.py文件中的函数调用会显示函数定义时的形参，方便你检查传参有没有传歪。  
另外，搜索`inlay Hints`有更多的类似设置，你可以尝试一下。
