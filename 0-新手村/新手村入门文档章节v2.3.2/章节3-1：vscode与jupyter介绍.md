# vscode与jupyter介绍

## vscode与pycharm的选择

vscode与pycharm是同类产品，都属于IDE（集成开发环境，集成了一些方便写代码和调试代码的工具）。因此pycharm和vscode二选一即可。

- vscode的特点是免费、轻量级、可高度自定义、多编程语言通用
- pycharm是python专用，只支持python一种语言，有免费版和付费版。

由于pycharm的内置jupyter是付费功能，而同样的功能在vscode免费，因此本教程选择将vscode作为推荐配置。

### vscode

- 类似conda/pip给python安装的各种第三方库，vscode的很多强大功能都来源于其丰富的扩展生态，如果没有安装任何扩展，则vscode就只是一个单纯的文本编辑器。
- vscode将终端页面嵌入到了编辑器中，也就是可以在vscode中使用powershell或cmd来操作conda，而不需要来回切换窗口。
  - vscode只是编写代码的工具，所以如果没有指定环境则vscode只能编写代码不能执行代码。
  - vscode是将终端页面内置到自身页面并通过控制终端启动conda虚拟环境的，所以vscode与conda的安装卸载互不影响。

### jupyter

- jupyter的基本功能是可以逐块运行代码和展示输出结果，适用于代码分析、教学展示、写笔记和分享。
- jupyter和vscode一样需要指定使用哪个conda的虚拟环境运行python代码。  
- 为了让conda的虚拟环境能够“配合”jupyter，需要在虚拟环境中额外安装“ipykernel”库，（每个需要使用jupyter的虚拟环境都要安装一次）。  
- jupyter既可以单独使用，也可以以插件的形式嵌入到vscode中
