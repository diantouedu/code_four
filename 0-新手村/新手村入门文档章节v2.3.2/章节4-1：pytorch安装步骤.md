# pytorch安装详细版

- 本章节分为GPU检查、GPU版torch安装、CPU版安装三部分，安装GPU版需阅读前两部分，安装CPU版只需要阅读最后一部分，读者可按需阅读。  
- 在安装torch前请先确保已有创建的虚拟环境并且已激活环境。  

## GPU科普

- 显卡（也叫GPU）一般有核心显卡（核显）、集成显卡（集显，已淘汰）、独立显卡（独显）三大类。显卡的功能是为电脑提供屏幕显示的内容。简而言之，没有显卡，电脑会黑屏。
    - 集显与核显：集显与核显都是使用CPU和电脑内存完成计算任务，核显是直接集成到了CPU核心中，集显则在CPU之外，但一般焊接在主板上，这两类显卡都无法从电脑上拆分出去。
    - 独显：有独立的计算和存储组件（显存），不需要使用CPU和电脑内存，独立完成计算任务，并且在大部分电脑上可以方便的单独安装或拆卸下来。市场上生产独显的两大厂商分别为NVIDIA和AMD，这两家公司生产的独立显卡分别被称为N卡和A卡，AI训练目前主流使用N卡。
- AI训练一般使用N卡，N卡在用于AI训练时的运行逻辑与作为显卡时的计算策略是不同的，并且不同型号的显卡的硬件结构也有所不同...总之最终导致的结果就是目前在安装GPU版torch时需要先查询自己显卡的cuda版本。

## GPU版torch和CPU版torch的选择

- GPU版torch只支持具有N卡的电脑，安装步骤根据显卡cuda版本和驱动版本的不同，安装步骤可能会相当繁琐。
- CPU版torch支持任何电脑，并且安装步骤方便，但是训练速度慢。
- 不要两种都安装。
- 选择安装CPU版可直接跳到本页最底部的CPU版torch安装步骤部分。
- 选择安装GPU版请继续按顺序阅读。

## 检查N卡GPU是否存在

首先确定自己的电脑有没有独立显卡。右键底部任务栏空白处->打开任务管理器->性能->GPU，检查是否存在NVIDIA开头的独立显卡。  

![GPU检查图一](img/GPU_check1.jpg)

![GPU检查图二](img/GPU_check2.jpg)

- 有NVIDIA开头的GPU：有N卡，可以正常安装cuda版pytorch
- 只有其他开头的GPU：无法安装cuda版，请选择安装CPU版
- 技术党参考：知道自己是A卡的用户也可以使用ROCM版pytorch，但是坑多且只支持Linux操作系统，非必要建议也安装CPU版。

## （可选）更新显卡驱动

少数长期不联网的电脑需要手动更新显卡驱动，这里提供更新方法  
在英伟达官网下载自动更新驱动的工具  
<https://www.nvidia.cn/geforce/drivers/>  
下载安装后使用工具更新驱动即可。  

## 检查自己的cuda版本

在powershell中输入

```powershell
nvidia-smi
```

### 指令成功执行

查看右上角自己的`CUDA Version`确定自己能安装的最高cuda版本，比如我的是12.2，所以我选择pytorch-cuda版本时不能大于12.2。  

![GPU检查图3](img/GPU_check3.jpg)

### 指令执行失败

绝大部分情况是由于电脑没有N卡，如果确定自己的电脑没有N卡，则只能安装CPU版torch

极少数情况下，该指令会被放置在一个随机命名的路径中导致无法在shell中识别
可用通过在C盘全局搜索`nvidia-smi.exe`来进行确认，将搜索到的路径添加到`path`中从而解决该问题。如果这一步操作对读者来说比较有难度，则还是建议安装CPU版torch，不要死磕。

## GPU版torch安装

进入pytorch官网：`https://pytorch.org/`并往下拉，找到以下页面：

![pytorch官网安装指令页面](img/pytorch_index.jpg)

- pytorch安装的cuda版本不能高于自己的`CUDA Version`，在此基础上在上方页面选择安装版本。建议选择conda安装，使用pip可能由于conda的机制造成意料之外的结果。
    - 意料之外的结果说明：conda在创建环境时默认不会立刻安装python，因此新的环境中不存在pip，使用pip指令时会由于环境变量的优先级机制，匹配到上一层环境的pip，因此使用pip指令在新环境安装时可能会将库安装到错误的环境中。避免此问题的方法是：在新环境使用pip之前先执行`conda install python`安装python。
- 国内用户使用conda安装pytorch时需要在换源后需要删除其中的`-c pytorch`方能正常使用镜像源下载。  
    - 注意镜像源经常会匹配版本失败导致在指定为cuda版本的前提下依然下载成CPU版。有条件的建议切换成国外网络环境。  
- 如果自己的cuda版本过低，可以点击上图左下角的`previous versions of PyTorch`按钮查找不高于自己cuda版本的其他版本安装指令。  
    - 该页面同时存在conda与pip的安装指令，考虑到当前使用国内源下载pytorch的不稳定性，建议尽可能将自己的电脑的网络切换为国外的网络环境以避免出现更复杂的问题。  
    - 使用国内源安装时需要删除`--extra-index-url https://download.pytorch.org/whl/cu117`等类似后缀，或将其替换为国内源的链接。  

GPU版torch主流版本安装指令：

```powershell
# cuda版本大于等于11.8
conda install pytorch torchvision torchaudio pytorch-cuda=12.4 -c pytorch -c nvidia
conda install pytorch torchvision torchaudio pytorch-cuda=12.1 -c pytorch -c nvidia
conda install pytorch torchvision torchaudio pytorch-cuda=11.8 -c pytorch -c nvidia
# cuda版本等于11.7
conda install pytorch==2.0.1 torchvision==0.15.2 torchaudio==2.0.2 pytorch-cuda=11.7 -c pytorch -c nvidia
# cuda版本等于11.6

```

## CPU版torch安装

要求：

- 已正常安装conda并按要求配置好镜像源（如果需要）。
- 已激活虚拟环境。

```powershell
# 国内已配置镜像源用户使用：
conda install pytorch torchvision torchaudio cpuonly
# 国外网络环境用户使用：
conda install pytorch torchvision torchaudio cpuonly -c pytorch
```

## 检查pytorch安装情况

pytorch安装完成后，>>**重启powershell**<<然后输入下方指令并稍等片刻，返回cuda支持情况以确定pytorch是否安装成功。

```powershell
python -c "import torch;print(torch.cuda.is_available())"
# 安装GPU版pytorch返回应当为True  
# 安装CPU版pytorch返回应当为False  
```

```powershell
python -c "import torch;print(torch.__version__)"
# 正常情况应当返回torch版本2.x.x，过旧的cuda版本可能会是1.x.x
```

若输出符合预期则环境配置完毕。若输出与预期不符则可能为环境配置步骤中的某步出问题，请检查。
