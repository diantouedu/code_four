# conda常用指令说明

## 说明

conda的使用方法主要是在终端shell中输入指令进行。
本节内容列出了最常用的实用指令，主要用于查阅，极不建议背诵。

## 环境管理

创建环境

```powershell
conda create -n 环境名 #创建新的空环境
conda create -n 环境名 python=版本号 numpy scipy matplotlib pandas #创建指定版本的python环境，并安装指定包。
conda create -n 新环境名 --clone 旧环境名 #克隆一个环境，注意本命令执行速度很慢，请耐心等待，推荐手动复制一个环境并重命名
```

**注意：第一个指令创建环境时不包含python，但conda会在安装python包时自动安装python，所以很多人不知道这种情况，导致在conda与pip混用时如果在创建环境后直接使用pip安装包安装到base环境！使用pip前一定要给虚拟环境安装python！**  

```powershell
# 列出环境
conda info --envs
# 或者
conda env list

# 激活与退出环境
conda activate 环境名 #激活环境
conda deactivate #退出环境

#删除环境
conda remove --name 环境名 --all
conda env remove -n 环境名
```

## 包管理

本节加入了pip指令以作为对比  

列出当前环境中的包

```powershell
conda list
pip list
# 在当前环境安装包
conda install 包名,包名2,包名3 # 英文逗号分隔
pip install 包名 包名2 包名3 # 空格分隔
# 同时安装多个包可以同时处理多个包的冲突，是很推荐的做法。
conda install 包名=版本号
pip install 包名==版本号 # 注意pip需要两个等号
# 清理包缓存
conda clean --all
pip cache purge
# 使用requirements.txt文件安装包
conda install --file requirements.txt
pip install -r requirements.txt
# 讨厌conda每次都要输入y确认的话，可以直接在命令上增加`--yes`修饰，例如
conda install numpy --yes
# 检查环境中存在的冲突
pip check
```

## conda官方中文文档

更多命令请参阅：  
<https://docs.conda.org.cn/projects/conda/en/stable/commands/index.html>
