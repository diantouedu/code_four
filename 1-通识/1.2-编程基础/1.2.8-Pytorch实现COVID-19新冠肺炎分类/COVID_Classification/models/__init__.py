from .mobilenet_v2 import mobilenetv2
from .resnet import resnet18, resnet50
from .shufflenet_v2 import shufflenet_v2_x1_0

model_dict = {
    'resnet18': resnet18,
    'resnet50': resnet50,
    'mobilenetv2': mobilenetv2, 
    'shufflenetv2': shufflenet_v2_x1_0
}

def create_model(model_name, num_classes):   
    return model_dict[model_name](num_classes = num_classes)