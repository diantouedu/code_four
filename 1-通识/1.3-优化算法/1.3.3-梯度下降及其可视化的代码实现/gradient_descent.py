import math

# 定义问题
def question(ipt):
    return ipt ** 2 + math.sin(5 * ipt)

# 找导数/梯度
def grad_func(ipt):
    return 2 * ipt + 5 * math.cos(5 * ipt)

# 移步函数
def dealer(ipt_x, gradient, learning_rate):
    # 打印用
    print(f'current input: {ipt_x}')
    print(f'current gradient: {gradient}')
    print(f'current learning rate: {learning_rate}')

    # 功能
    # 走一步
    ipt_x -= learning_rate * gradient
    # 这一步好不好
    y = question(ipt_x)

    return ipt_x, y


# 超参 hyperparameter
x = float(input('start point: \n'))
lr = float(input('learning rate: \n'))
iteration_number = int(input('number of iterations: \n'))
extra_move = int(input('extra move after finding the local minimum: \n'))

start_lr = lr

# 局部最优
local_x = x
local_y = question(x)

# 保存找到的x和y
x_record = []
y_record = []
gradient_record = []

i = 0
while i < iteration_number:
    # 你的extra move 走太多了，有点停不下来
    # if i > iteration_number * 1.1:
    #     break

    # 记录xy
    x_record.append(x)
    y_record.append(question(x))

    # 算梯度
    gradient = grad_func(x)

    gradient_record.append(gradient)

    # 对于梯度为0，局部最优的强行调整
    if math.fabs(gradient) < 0.005:
        local_x = x
        local_y = question(x)
        iteration_number += extra_move

        if gradient > 0:
            gradient += 1
            x -= 1
        else:
            gradient -= 1
            x += 1

        lr = start_lr


    # 走一步
    x, current_y = dealer(x, gradient, lr)

    if current_y < local_y:
        local_x = x
        local_y = current_y
        # 如果当前找到了更好的，就减小步长，走小一点
        lr *= 0.97
    else:
        # 如果，当前没有更好，则周围可能也没有特别好
        lr *= 1.03

    # if i % 10 == 0:
    #     lr *= 0.99

    i += 1

print(f' The minimum found is: {local_y}')

import matplotlib.pyplot as plt

plt.plot(x_record, y_record)
plt.show()

plt.plot(range(0, iteration_number), x_record)
plt.show()

plt.plot(range(0, iteration_number), y_record)
plt.show()

plt.plot(range(0, iteration_number), gradient_record)
plt.show()





