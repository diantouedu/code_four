import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# 导入波士顿房价数据集
from sklearn.datasets import load_boston

# 导入数据集并进行预处理
def load_data():
    boston = load_boston()
    df = pd.DataFrame(boston.data, columns=boston.feature_names)
    df['PRICE'] = boston.target
    X = df.drop('PRICE', axis=1).values
    y = df['PRICE'].values
    return X, y

# 数据标准化
def normalize(X):
    mean = np.mean(X, axis=0)
    std = np.std(X, axis=0)
    X_normalized = (X - mean) / std
    return X_normalized

# 添加偏置项
def add_bias(X):
    ones = np.ones((X.shape[0], 1))
    X_with_bias = np.concatenate((ones, X), axis=1)
    return X_with_bias

# 计算均方误差
def mean_squared_error(y_true, y_pred):
    return np.mean((y_true - y_pred) ** 2)

# 梯度下降训练模型
def train(X, y, learning_rate=0.01, num_iterations=1000):
    num_samples, num_features = X.shape
    weights = np.zeros(num_features)
    bias = 0
    losses = []

    for _ in range(num_iterations):
        y_pred = np.dot(X, weights) + bias
        dw = (1 / num_samples) * np.dot(X.T, (y_pred - y))  # 计算权重梯度
        db = (1 / num_samples) * np.sum(y_pred - y)  # 计算偏执b梯度
        weights -= learning_rate * dw  # w梯度下降
        bias -= learning_rate * db  # b梯度下降
        loss = mean_squared_error(y, y_pred)
        losses.append(loss)

    return weights, bias, losses

# 预测
def predict(X, weights, bias):
    y_pred = np.dot(X, weights) + bias
    return y_pred

# 可视化训练过程
def plot_training_process(losses):
    plt.plot(losses)
    plt.xlabel('Iteration')
    plt.ylabel('Mean Squared Error')
    plt.title('Training Process')
    plt.show()

# 可视化预测结果
def plot_predictions(y_true, y_pred, weights, bias):
    df = pd.DataFrame({'True': y_true, 'Predicted': y_pred})
    sns.scatterplot(data=df, x='True', y='Predicted')

    # 绘制拟合直线
    x_line = np.linspace(min(y_true), max(y_true), 100)
    y_line = weights[1] * x_line + bias   # weights中括号里面的数字可以改，代表使用某个数值作为x
    plt.plot(x_line, y_line, color='r', label='Fitted Line')

    plt.xlabel('True Price')
    plt.ylabel('Predicted Price')
    plt.title('True vs Predicted Prices with Fitted Line')
    plt.legend()
    plt.show()


# 主函数
def main():
    # 加载数据集
    X, y = load_data()

    # 数据预处理
    X_normalized = normalize(X)
    X_with_bias = add_bias(X_normalized)

    # 训练模型
    weights, bias, losses = train(X_with_bias, y, learning_rate=0.01, num_iterations=1000)

    # 可视化训练过程
    plot_training_process(losses)

    # 进行预测
    y_pred = predict(X_with_bias, weights, bias)

    # 可视化预测结果（包括拟合直线）
    plot_predictions(y, y_pred, weights[1:], bias)


if __name__ == '__main__':
    main()