import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from sklearn.datasets import load_boston
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn import metrics
from sklearn import preprocessing

data = load_boston()
data_pd = pd.DataFrame(data.data, columns=data.feature_names)
data_pd['price'] = data.target

# 查看数据类型
# print(data_pd.describe())

# 查看空缺值
# print(data_pd.isnull().sum())

# print(data_pd.head())
# 为选取合适的变量作为自变量，计算每一个特征和price的相关系数
# print(data_pd.corr()['price'])

corr = data_pd.corr()
corr = corr['price']
# 将绝对值大于0.5的特征绘制出来
corr[abs(corr) > 0.5].sort_values().plot.bar()

# 绘制LSTAT和price的散点图
data_pd.plot(kind="scatter", x="LSTAT", y="price")

# 绘制PTRATIO和price的散点图
data_pd.plot(kind="scatter", x="PTRATIO", y="price")

# 绘制RM和price的散点图
data_pd.plot(kind="scatter", x="RM", y="price")

# plt.show()

# 制作训练集和测试集的数据
feature_cols = ['RM']  # LSTAT  PTRATIO  RM
X = data_pd[feature_cols]
Y = data_pd['price']

# 分割训练集和测试集
train_X, test_X, train_Y, test_Y = train_test_split(X, Y, test_size=0.2)
# print(Y.describe())

# 加载模型
linreg = LinearRegression()
# 拟合数据
linreg.fit(train_X, train_Y)

print(linreg.intercept_)

# pair the feature names with the coefficients
b = list(zip(feature_cols, linreg.coef_))
print(b)

# 进行预测
y_predict = linreg.predict(test_X)
# 计算均方根误差
print("均方根误差=", metrics.mean_squared_error(y_predict, test_Y))


import seaborn as sns  # seaborn就是在matplot的基础上进行了进一步封装

sns.lmplot(x='LSTAT', y='price', data=data_pd, aspect=1.5, scatter_kws={'alpha':0.2})

plt.show()
