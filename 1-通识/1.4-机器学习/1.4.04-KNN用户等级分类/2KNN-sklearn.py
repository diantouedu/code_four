import random
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.font_manager import FontProperties

def view(data):
    # 设置中文字体
    font = FontProperties(fname='C:/Windows/Fonts/simsun.ttc')  # 将字体文件路径替换为您系统中的中文字体文件路径

    # 提取数据
    x = [item[0] for item in data]  # 总消费额度
    y = [item[1] for item in data]  # 当年消费额度
    z = [item[2] for item in data]  # 来访次数
    labels = [item[3] for item in data]  # 用户等级

    # 创建三维图形
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # 根据用户等级设置不同颜色
    colors = ['r', 'g', 'b']
    c = [colors[label] for label in labels]

    # 绘制散点图，设置颜色
    ax.scatter(x, y, z, c=c)

    # 设置坐标轴标签
    ax.set_xlabel('总消费额度', fontproperties=font)
    ax.set_ylabel('当年消费额度', fontproperties=font)
    ax.set_zlabel('来访次数', fontproperties=font)

    # 显示图形
    plt.show()

def main(data):
    train_data = data

    random.shuffle(train_data)  # 随机打乱训练数据

    train_size = int(0.7 * len(train_data))  # 计算训练集大小
    train_set = train_data[:train_size]  # 划分训练集
    validation_set = train_data[train_size:]  # 划分验证集

    X_train = np.array([instance[:-1] for instance in train_set])  # 训练集特征
    y_train = np.array([instance[-1] for instance in train_set])  # 训练集标签

    k = 3

    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(X_train, y_train)

    X_validation = np.array([instance[:-1] for instance in validation_set])  # 验证集特征
    y_validation = np.array([instance[-1] for instance in validation_set])  # 验证集标签

    accuracy = knn.score(X_validation, y_validation)
    print("Accuracy:", accuracy)

    user_info = np.array([(300, 70, 50)])  # 设置用户信息
    predicted_class = knn.predict(user_info)
    print("该用户属于类别:", predicted_class[0])

if __name__ == '__main__':
    data = [
        (50, 20, 10, 0), (201, 20, 20, 0), (207, 30, 17, 0),
        (298, 0, 0, 0), (150, 40, 10, 0), (106, 40, 30, 0),
        (57, 20, 10, 0), (251, 60, 29, 0), (267, 30, 16, 0),
        (248, 10, 20, 0), (158, 40, 11, 0), (176, 60, 34, 0),
        (238, 20, 5, 0), (170, 40, 17, 0), (166, 50, 31, 0),

        (350, 90, 60, 1), (403, 60, 25, 1), (469, 80, 1, 1),
        (1000, 10, 8, 1), (567, 130, 12, 1), (600, 50, 45, 1),
        (350, 90, 60, 1), (443, 64, 21, 1), (469, 79, 1, 1),
        (960, 20, 28, 1), (389, 120, 16, 1), (600, 50, 45, 1),
        (590, 90, 38, 1), (343, 110, 72, 1), (600, 66, 49, 1),

        (850, 260, 18, 2), (923, 210, 12, 2), (708, 267, 31, 2),
        (789, 270, 17, 2), (857, 340, 55, 2), (883, 255, 32, 2),
        (850, 260, 18, 2), (975, 210, 12, 2), (730, 265, 33, 2),
        (789, 270, 33, 2), (833, 344, 55, 2), (880, 257, 27, 2),
        (790, 210, 7, 2), (968, 484, 35, 2)
    ]

    main(data)
    # view(data)