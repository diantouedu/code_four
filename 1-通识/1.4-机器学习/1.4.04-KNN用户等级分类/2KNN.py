import math
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.font_manager import FontProperties

# 计算欧氏距离
def euclidean_distance(x1, x2):
    distance = 0
    for i in range(len(x1)):
        distance += pow((x1[i] - x2[i]), 2)
    return math.sqrt(distance)

# 获取k个最近邻的标签
def get_neighbors(train_set, test_instance, k):
    distances = []
    for train_instance in train_set:  # 遍历所有的用户
        dist = euclidean_distance(test_instance[:-1], train_instance[:-1])  # 一次传用户的一个信息
        distances.append((train_instance, dist))
    distances.sort(key=lambda x: x[1])  # 按距离升序排序
    neighbors = [item[0] for item in distances[:k]]
    return neighbors

# 根据邻居的标签进行投票
def vote(neighbors):
    class_votes = {}  # 创建类别dict
    for neighbor in neighbors:
        label = neighbor[-1]  # 取neighbor标签
        if label in class_votes:
            class_votes[label] += 1  # 该class标签以存在则dict的value值加一
        else:
            class_votes[label] = 1  # 第一次出现的label其value为1
    sorted_votes = sorted(class_votes.items(), key=lambda x: x[1], reverse=True)  # 对class_votes进行降序排序
    return sorted_votes[0][0]  # 返回降序排序之后的第一个label

# 预测单个样本的类别
def predict(train_set, test_instance, k):
    neighbors = get_neighbors(train_set, test_instance, k)  # 获取k个最近邻的标签
    predicted_class = vote(neighbors)  # 根据邻居的标签进行投票
    return predicted_class

def view(data):
    # 设置中文字体
    font = FontProperties(fname='C:/Windows/Fonts/simsun.ttc')  # 将字体文件路径替换为您系统中的中文字体文件路径

    # 提取数据
    x = [item[0] for item in data]  # 总消费额度
    y = [item[1] for item in data]  # 当年消费额度
    z = [item[2] for item in data]  # 来访次数
    labels = [item[3] for item in data]  # 用户等级

    # 创建三维图形
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    # 根据用户等级设置不同颜色
    colors = ['r', 'g', 'b']
    c = [colors[label] for label in labels]

    # 绘制散点图，设置颜色
    ax.scatter(x, y, z, c=c)

    # 设置坐标轴标签
    ax.set_xlabel('总消费额度', fontproperties=font)
    ax.set_ylabel('当年消费额度', fontproperties=font)
    ax.set_zlabel('来访次数', fontproperties=font)

    # 显示图形
    plt.show()


# 主函数，使用提供的数据集进行训练和预测
def main(data, K):
    train_data = data

    random.shuffle(train_data)  # 随机打乱训练数据

    train_size = int(0.7 * len(train_data))  # 计算训练集大小
    train_set = train_data[:train_size]  # 划分训练集
    validation_set = train_data[train_size:]  # 划分验证集

    k = K

    correct_predictions = 0

    for test_instance in validation_set:
        predicted_class = predict(train_set, test_instance, k)    # 获取k个最近邻的标签
        if predicted_class == test_instance[-1]:  # 检查预测结果是否正确
            correct_predictions += 1  # 预测结果正确个数

    accuracy = correct_predictions / len(validation_set)  # 准确率
    print("Accuracy:", accuracy)  # 输出准确率

    user_info = (300, 70, 50)  # 设置用户信息
    predicted_class = predict(train_set, user_info, k)  # 预测新数据
    print("该用户属于类别:", predicted_class)  # 输出新数据属于哪一类别


if __name__ == '__main__':
    data = [
        (50, 20, 10, 0), (201, 20, 20, 0), (207, 30, 17, 0),
        (298, 0, 0, 0), (150, 40, 10, 0), (106, 40, 30, 0),
        (57, 20, 10, 0), (251, 60, 29, 0), (267, 30, 16, 0),
        (248, 10, 20, 0), (158, 40, 11, 0), (176, 60, 34, 0),
        (238, 20, 5, 0), (170, 40, 17, 0), (166, 50, 31, 0),

        (350, 90, 60, 1), (403, 60, 25, 1), (469, 80, 1, 1),
        (1000, 10, 8, 1), (567, 130, 12, 1), (600, 50, 45, 1),
        (350, 90, 60, 1), (443, 64, 21, 1), (469, 79, 1, 1),
        (960, 20, 28, 1), (389, 120, 16, 1), (600, 50, 45, 1),
        (590, 90, 38, 1), (343, 110, 72, 1), (600, 66, 49, 1),

        (850, 260, 18, 2), (923, 210, 12, 2), (708, 267, 31, 2),
        (789, 270, 17, 2), (857, 340, 55, 2), (883, 255, 32, 2),
        (850, 260, 18, 2), (975, 210, 12, 2), (730, 265, 33, 2),
        (789, 270, 33, 2), (833, 344, 55, 2), (880, 257, 27, 2),
        (790, 210, 7, 2), (968, 484, 35, 2)
    ]

    main(data, K=3)
    # view(data)

