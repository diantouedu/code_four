from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import numpy as np

# 定义决策树节点
class Node:
    def __init__(self, feature_idx=None, threshold=None, label=None, gini=None):
        """
        :param feature_idx: 特征id
        :param threshold: 划分阈值
        :param label: 标签
        :param gini: 基尼指数
        """
        self.feature_idx = feature_idx
        self.threshold = threshold
        self.label = label
        self.gini = gini
        self.left = None
        self.right = None

class CarTDecisionTree:
    def __init__(self, max_depth=100, min_samples_split=3):
        self.max_depth = max_depth
        self.min_samples_split = min_samples_split
        self.root = None

    def fit(self, X, y):
        self.root = self._build_tree(X, y, depth=0)

    def predict(self, X):
        y_pred = []
        for sample in X:
            y_pred.append(self._predict_sample(sample, self.root))
        return np.array(y_pred)


    def _build_tree(self, X, y, depth):
        if depth == self.max_depth or len(y) < self.min_samples_split or np.unique(y).size == 1:
            label = self._most_common_label(y)
            gini = self._gini_impurity(y)
            print(f"Leaf Node: Label={label}, Gini={gini:.4f}")
            return Node(label=label, gini=gini)

        best_feature, best_threshold, best_gini = self._find_best_split(X,y)
        left_indices = X[:, best_feature] <= best_threshold
        right_indices = X[:, best_feature] > best_threshold

        print(f"Internal Node: Feature={best_feature}, Threshold={best_threshold:.2f}, Gini={best_gini:.4f}")

        left_child = self._build_tree(X[left_indices], y[left_indices], depth+1)  # 递归
        right_child = self._build_tree(X[right_indices], y[right_indices], depth + 1)  # 递归

        node = Node(feature_idx=best_feature, threshold=best_threshold, gini=best_gini)
        node.left = left_child
        node.right = right_child

        return node

    def _find_best_split(self, X, y):
        best_feature = None
        best_threshold = None
        best_gini = 1.0
        for feature_idx in range(X.shape[1]):
            thresholds = np.unique(X[:, feature_idx])
            for threshold in thresholds:
                gini = self._gini_index(X, y, feature_idx, threshold)
                if gini < best_gini:
                    best_gini = gini
                    best_feature = feature_idx
                    best_threshold = threshold
        return best_feature, best_threshold, best_gini

    def _gini_index(self,X, y, feature_idx, threshold):
        left_indices = X[:, feature_idx] <= threshold
        right_indices = X[:, feature_idx] > threshold

        gini_left = self._gini_impurity(y[left_indices])
        gini_right = self._gini_impurity(y[right_indices])

        left_size = np.sum(left_indices)
        right_size = np.sum(right_indices)
        total_size = left_size + right_size

        gini = (left_size / total_size) * gini_left + (right_size / total_size) * gini_right
        return gini

    def _gini_impurity(self, y):
        _, counts = np.unique(y, return_counts=True)
        probabilities = counts / len(y)
        gini = 1.0 - np.sum(probabilities ** 2)
        return gini

    def _most_common_label(self, y):
        labels, counts = np.unique(y, return_counts=True)
        most_common_lable = labels[np.argmax(counts)]
        return most_common_lable
    def _predict_sample(self, sample, node):
        if node.label is not None:
            return node.label

        if sample[node.feature_idx] <= node.threshold:
            return self._predict_sample(sample, node.left)
        else:
            return self._predict_sample(sample, node.right)

# 加载数据集
iris = load_iris()
X = iris.data
y= iris.target

# 将数据集划分为训练集和测试集
X_train, X_test, y_trian, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# 创建决策树
CLF = CarTDecisionTree()

CLF.fit(X_train,y_trian)
