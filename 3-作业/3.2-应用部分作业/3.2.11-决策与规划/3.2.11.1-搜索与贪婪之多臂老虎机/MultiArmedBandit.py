import numpy as np

# 定义多臂老虎机类
class MultiArmedBandit:
    def __init__(self, num_bandits):
        self.num_bandits = num_bandits
        self.bandit_means = np.random.normal(0, 1, num_bandits)  # 每个老虎机的平均奖励
        self.bandit_counts = np.zeros(num_bandits)  # 每个老虎机的被选择次数
        self.total_reward = 0

    def pull_bandit(self, bandit_idx):
        reward = np.random.normal(self.bandit_means[bandit_idx], 1)  # 从老虎机中拉动臂获取奖励
        self.total_reward += reward
        self.bandit_counts[bandit_idx] += 1
        return reward

    def epsilon_greedy(self, epsilon):
        if np.random.random() < epsilon:
            # 探索：随机选择一个老虎机
            bandit_idx = np.random.randint(0, self.num_bandits)
        else:
            # 利用：选择平均奖励最高的老虎机
            bandit_idx = np.argmax(self.bandit_means)
        reward = self.pull_bandit(bandit_idx)
        return reward

# 初始化多臂老虎机
num_bandits = 3
bandit = MultiArmedBandit(num_bandits)

# ε-贪心算法参数
epsilon = 0.1
num_steps = 1000

# 运行ε-贪心算法
for _ in range(num_steps):
    reward = bandit.epsilon_greedy(epsilon)

# 打印每个老虎机的被选择次数和平均奖励
for i in range(num_bandits):
    print(f"Bandit {i + 1}: Count={bandit.bandit_counts[i]}, Mean Reward={bandit.bandit_means[i]}")

print(f"Total Reward: {bandit.total_reward}")