import logging
import os

import numpy as np
import torch
import torch.nn.functional as F
from PIL import Image
from torchvision import transforms

from utils.data_loading import BasicDataset
from unet import UNet
from utils.utils import plot_img_and_mask


def predict_img(net,
                full_img,
                device,
                scale_factor=1,
                out_threshold=0.5):  # 置信度阈值
    net.eval()
    img = torch.from_numpy(BasicDataset.preprocess(None, full_img, scale_factor, is_mask=False))
    img = img.unsqueeze(0)
    img = img.to(device=device, dtype=torch.float32)

    with torch.no_grad():
        output = net(img).cpu()
        output = F.interpolate(output, (full_img.size[1], full_img.size[0]), mode='bilinear')
        if net.n_classes > 1:
            mask = output.argmax(dim=1)
        else:
            mask = torch.sigmoid(output) > out_threshold

    return mask[0].long().squeeze().numpy()


def get_output_filename(input_file):
    return f'{os.path.splitext(input_file)[0]}_OUT.png'


def mask_to_image(mask: np.ndarray, mask_values):
    if isinstance(mask_values[0], list):
        out = np.zeros((mask.shape[-2], mask.shape[-1], len(mask_values[0])), dtype=np.uint8)
    elif mask_values == [0, 1]:
        out = np.zeros((mask.shape[-2], mask.shape[-1]), dtype=bool)
    else:
        out = np.zeros((mask.shape[-2], mask.shape[-1]), dtype=np.uint8)

    if mask.ndim == 3:
        mask = np.argmax(mask, axis=0)

    for i, v in enumerate(mask_values):
        out[mask == i] = v

    return Image.fromarray(out)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

    # 硬编码参数
    model_path = 'checkpoints/checkpoint_epoch5.pth'
    input_files = ['data/imgs/0cdf5b5d0ce1_03.jpg']  # 在此处输入图片路径
    output_dir = 'output'  # 输出文件夹名称
    viz = False  # 是否可视化结果
    no_save = False  # 是否保存输出的掩膜图像
    mask_threshold = 0.5  # 掩膜像素值阈值
    scale_factor = 0.5  # 输入图像的缩放因子
    bilinear = False  # 是否使用双线性插值
    num_classes = 2  # 类别数

    net = UNet(n_channels=3, n_classes=num_classes, bilinear=bilinear)

    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    logging.info(f'Loading model {model_path}')
    logging.info(f'Using device {device}')

    net.to(device=device)
    state_dict = torch.load(model_path, map_location=device)
    mask_values = state_dict.pop('mask_values', [0, 1])
    net.load_state_dict(state_dict)

    logging.info('Model loaded!')

    for i, filename in enumerate(input_files):
        logging.info(f'Predicting image {filename} ...')
        img = Image.open(filename)

        mask = predict_img(net=net,
                           full_img=img,
                           device=device,
                           scale_factor=scale_factor,
                           out_threshold=mask_threshold)

        if not no_save:
            os.makedirs(output_dir, exist_ok=True)
            out_filename = os.path.join(output_dir, get_output_filename(os.path.basename(filename)))
            result = mask_to_image(mask, mask_values)
            result.save(out_filename)
            logging.info(f'Mask saved to {out_filename}')

        if viz:
            logging.info(f'Visualizing results for image {filename} ...')
            plot_img_and_mask(img, mask)

        # 输出预测图片路径
        logging.info(f'Prediction saved to {out_filename}')