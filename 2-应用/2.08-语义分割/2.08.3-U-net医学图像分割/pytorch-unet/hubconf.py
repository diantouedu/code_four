# hubconf.py 文件的作用是充当 PyTorch Hub 的配置文件，用于定义如何在 PyTorch Hub 中加载和使用该 UNet 模型
import torch
from unet import UNet as _UNet

def unet_carvana(pretrained=False, scale=0.5):
    """
    在Carvana数据集上训练的UNet模型（https://www.kaggle.com/c/carvana-image-masking-challenge/data）。
    在预测时，将scale设置为0.5（50%）。
    """
    net = _UNet(n_channels=3, n_classes=2, bilinear=False)
    if pretrained:
        if scale == 0.5:
            checkpoint = 'https://github.com/milesial/Pytorch-UNet/releases/download/v3.0/unet_carvana_scale0.5_epoch2.pth'
        elif scale == 1.0:
            checkpoint = 'https://github.com/milesial/Pytorch-UNet/releases/download/v3.0/unet_carvana_scale1.0_epoch2.pth'
        else:
            raise RuntimeError('Only 0.5 and 1.0 scales are available')
        state_dict = torch.hub.load_state_dict_from_url(checkpoint, progress=True)
        if 'mask_values' in state_dict:
            state_dict.pop('mask_values')
        net.load_state_dict(state_dict)

    return net

