import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QVBoxLayout, QWidget, QPushButton, QFileDialog, QLineEdit
from PyQt5.QtGui import QPixmap
import os
import json
import torch.nn as nn
from AlexNet import AlexNet
import torch
from PIL import Image
from torchvision import transforms


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("图片上传和展示")
        self.setGeometry(100, 100, 400, 300)

        # 创建标签、按钮和文本框
        self.image_label = QLabel(self)
        self.upload_button = QPushButton("上传图片", self)
        self.label_textbox = QLineEdit(self)

        # 设置标签、按钮和文本框的位置和大小
        self.image_label.setGeometry(10, 10, 380, 200)
        self.upload_button.setGeometry(150, 220, 100, 30)
        self.label_textbox.setGeometry(10, 250, 380, 30)

        # 绑定按钮的点击事件
        self.upload_button.clicked.connect(self.upload_image)

    def upload_image(self):
        # 打开文件对话框选择图片文件
        file_dialog = QFileDialog()
        image_path, _ = file_dialog.getOpenFileName(self, "选择图片", "", "Images (*.png *.xpm *.jpg *.bmp)")

        # 加载并显示选中的图片
        pixmap = QPixmap(image_path)
        self.image_label.setPixmap(pixmap.scaled(self.image_label.size(), aspectRatioMode=True))

        # 调用人工智能模型获取图片标签
        label = self.get_image_label(image_path)
        self.label_textbox.setText(label)

    def get_image_label(self, image_path):
        # 加载模型和类别标签
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        data_transform = transforms.Compose([
            transforms.Resize((224, 224)),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        # 加载图片
        img = Image.open(image_path)
        img = data_transform(img)
        img = torch.unsqueeze(img, dim=0)
        img = img.to(device)

        # 加载类别标签
        class_indict ={
            "0": "daisy",
            "1": "dandelion",
            "2": "roses",
            "3": "sunflowers",
            "4": "tulips"
        }

        # 创建模型并加载权重
        model = AlexNet(num_classes=5).to(device)
        weights_path = "E:/PythonProject/AlexNetAPP/checkpoints/alex/alex_flower.pth"
        assert os.path.exists(weights_path), "file: '{}' dose not exist.".format(weights_path)

        model.load_state_dict(torch.load(weights_path))
        model.eval()

        # 使用模型进行预测
        with torch.no_grad():
            output = torch.squeeze(model(img)).cpu()
            predict = torch.softmax(output, dim=0)
            predict_cla = torch.argmax(predict).numpy()

        label = class_indict[str(predict_cla)]
        return label


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())